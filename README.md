cwmtui

This project aims to build a text based interface for Connectwise Manage.
The goal is to speed up the day to day usage for a technician.

## Milestones

1. Build a simple dashboard. Should show the current time and the most recent ticket on the service board. Chime when new tickets are found.
2. Show only most recent ticket relavant to the technician.
3. Bring SLA into account. The technician should be made aware of tickets that are approaching a SLA violation.


## Notes

Textual Library
https://github.com/Textualize/textual

Textual input widgets
https://github.com/sirfuzzalot/textual-inputs
